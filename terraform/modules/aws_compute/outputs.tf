output "ec2_public_ip" {
    value = aws_instance.myapp_server.public_ip
}

output "web_loadbalancer_url" {
  value = aws_elb.web.dns_name
}