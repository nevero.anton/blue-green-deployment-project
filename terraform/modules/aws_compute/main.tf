#-------------------------------------------------
# My Terraform module for compute
# Provision:
#
# - Launch Configuration with Auto AMI Lookup
# - Auto Scaling Group using 2 Availability Zones
# - Classic Load Balancer in 2 Availability Zones
#
# Made by Anton Nevero. February 2023
#--------------------------------------------------

/*resource "aws_instance" "myapp_server" {
  ami = data.aws_ami.latest_ubuntu.id
  instance_type = var.instance_type

  subnet_id = var.my-subnet-1
  vpc_security_group_ids = [var.my_sg]
  availability_zone = var.avail_zone

  associate_public_ip_address = true
  key_name = var.key_name
  
  user_data = file("entry-script.sh")

  tags = {
    Name = "${var.env_prefix}-server"
  }
}*/

resource "aws_launch_configuration" "web" {
  name  = "WebServer-Highly-Available-LC"
  image_id = data.aws_ami.latest_ubuntu.id
  instance_type = var.instance_type
  security_groups = [var.my_sg]
  user_data = file("entry-script.sh")
  key_name = "Ubuntu"
  associate_public_ip_address = true

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "web" {
  name = "WebServer-Highly-Available-ASG"
  launch_configuration = aws_launch_configuration.web.name
  min_size = 2
  max_size = 2
  min_elb_capacity = 2
  health_check_type = "ELB"
  load_balancers = [aws_elb.web.name]
  vpc_zone_identifier = [aws_default_subnet.default_az1.id, aws_default_subnet.default_az2.id]
  tags = {
    Name = "WebServer-Highly-Available-ASG"
    propagate_at_launch = true
  }
 
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_elb" "web" {
  name = "WebServer-HA-ELB"
  availability_zones = [data.aws_availability_zones.avail_zone.names[0], data.aws_availability_zones.avail_zone.names[1]]
  security_groups = [var.my_sg]
  listener {
    lb_port = 80
    lb_protocol = "http"
    instance_port = 80
    instance_protocol = "http"
  }
    health_check {
      healthy_threshold = 2
      unhealthy_treshold = 2
      timeout = 3
      target = "HTTP:80/"
      interval = 10
  }
  tags = {
    Name = "WebServer-Highly-Available-ELB"
  }
}

resource "aws_default_subnet" "default_az1"{
  availability_zone = data.aws_availability_zones.avail_zone.names[0]
}

resource "aws_default_subnet" "default_az2"{
  availability_zone = data.aws_availability_zones.avail_zone.names[1]
}


